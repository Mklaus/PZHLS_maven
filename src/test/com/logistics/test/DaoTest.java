package com.logistics.test;

import com.alibaba.fastjson.JSONObject;
import com.logistics.crypto.CryptoBase;
import com.logistics.crypto.RSACrypto;
import com.logistics.crypto.RSAKeyCache;
import com.logistics.dao.*;
import com.logistics.entity.*;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Arrays;


import static com.logistics.crypto.CryptoBase.byteToHex;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:applicationContext-common.xml"})
public class DaoTest{

    @Autowired
    private AdminDao adminDao;

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private CargoDao cargoDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private CarBusinessDao carBusinessDao;

    @Autowired
    private OilBuyDao oilBuyDao;

    @Autowired
    private CarrierBusinessDao cbDao;

    private Session session;

    @Test
    public void testAdminDao(){
        Admin admin = new Admin("admin","123".getBytes());
        adminDao.save(admin);
    }


    @Test
    public void testSaveGoods(){
        for (int i = 0; i < 10; i++){
            Goods goods = new Goods("name"+i,"type"+i);
            goods.setPrice(i*2);
            goods.setCount(i);
            goodsDao.save(goods);
        }
    }

    @Test
    public void testSaveCargo(){

        for (int i=0;i<10;i++){
            Cargo cargos = new Cargo(155+i,"大熊猫",1+i);
            cargoDao.save(cargos);
        }
    }
    

    @Test
    public void testSaveCarBusiness(){

        for (int i=0;i<10;i++){
            CarBusiness cb = new CarBusiness();
            cb.setDateOfStarting(1010+i);
            carBusinessDao.save(cb);
        }
    }

    @Test
    public void testSaveOilBuy(){
        for(int i=0;i<10;i++){
            OilBuy ob = new OilBuy();
            ob.setLitre(4 + i);
            ob.setPrice(1 + i * 4);
            oilBuyDao.save(ob);
        }
    }

    @Test
    public void testSaveCB(){
        for(int i=0;i<10;i++){
            CarrierBusiness cb = new CarrierBusiness();
            cbDao.save(cb);
        }
    }

}
