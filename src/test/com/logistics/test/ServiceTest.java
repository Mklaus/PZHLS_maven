package com.logistics.test;

import com.logistics.dao.GoodsRecordDao;
import com.logistics.dao.OilBuyDao;
import com.logistics.entity.*;
import com.logistics.service.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.UnsupportedEncodingException;
import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:applicationContext-common.xml"})
public class ServiceTest {

    @Autowired
    private AdminService adminServiceImpl;
    @Autowired
    private StaffService staffService;
    @Autowired
    private GoodsRecordDao goodsRecordDao;
    @Autowired
    private OilBuyDao oilBuyDao;
    @Autowired
    private ReportService reportService;
    @Autowired
    private CarBusinessService carBusinessService;
    @Autowired
    private VehicleService vehicleService;
    @Autowired
    private VehicleTypeService vehicleTypeService;
    @Autowired
    private TransportTaskService transportTaskService;
    @Autowired
    private CargoService cargoService;
    @Autowired
    private UserService userService;

    @Test
    // 测试添加用户
    public void testSave() {
        int[] cargoIdArray=new int[2];
        TransportTask transportTask=new TransportTask();
        transportTask.setStartDate(new GregorianCalendar(2015, 8, 22).getTimeInMillis());
        transportTask.setEndDate(new GregorianCalendar(2015, 8, 23).getTimeInMillis());
        transportTask.setCustomer(userService.get(1));
        transportTask.setStartPlace("广州");
        transportTask.setEndPlace("成都");
        cargoIdArray[0]= 1;
        cargoIdArray[1]= 2;
    }

    @Test
    public void testaave() {
        Admin admin = null;
        try {
            admin = new Admin("123","123".getBytes("UTF-8"));
            adminServiceImpl.register(admin);
            //	adminServiceImpl.login(admin);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
