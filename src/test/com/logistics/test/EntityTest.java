package com.logistics.test;


import com.logistics.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:applicationContext-common.xml"})
public class EntityTest {

	private Session session;

	@Before
	public void initSession() {
//		this.session = HibernateUtil.getSessionFactory().getCurrentSession();
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext-common.xml");
		SessionFactory sessionFactory = (SessionFactory) context.getBean("sessionFactory");
		session = sessionFactory.openSession();
		if (session != null){
			System.out.println("you");
		}
	}

	@Test
	public void test(){
		session.beginTransaction();

		CRUD(session);

		session.getTransaction().commit();
		session.close();
	}

	public void CRUD(Session session){
		int choose = 1;
		Vehicle v;
		VehicleType vt;

		switch (choose){
			case 0:
				vt = new VehicleType();
				vt.setDescription("van");
				v = new Vehicle();
				v.setCarNumber("123dsa");
				v.setVehicleType(vt);
				session.save(v);
				session.save(vt);
				break;

			case 1:
				v = (Vehicle)session.get(Vehicle.class,3);
				session.delete(v);
				break;

			case 2:
				vt = (VehicleType)session.get(VehicleType.class,1);
				session.delete(vt);
				break;

			case 3:
				v = new Vehicle();
				v.setCarNumber("fsdf324");
				vt = (VehicleType)session.get(VehicleType.class,3);
				v.setVehicleType(vt);
				session.save(v);
				break;
		}
	}


	@Test
	// 测试添加用户
	public void testSave() {
		session.beginTransaction();
		Admin admin = new Admin("test","haha".getBytes());
		session.save(admin);
		session.getTransaction().commit();
		session.close();
	}

	@Test
	public void testCascade() {
		session.beginTransaction();

		Address a1 = new Address("sichuan",1);
		Address a2 = new Address("chengdu",2);
		a2.setParent(a1);

		session.save(a1);
		session.save(a2);
		session.getTransaction().commit();
		session.close();
	}

	@Test
	public void addSon(){
		session.beginTransaction();

//		Staff driver = new Staff();
//		driver.setRealName("xiejinye");
//		Vehicle v = new Vehicle();
//		v.setCarNumber("qwe123");
//		v.setDriver(driver);
//
//		session.save(driver);
//		session.save(v);

		Staff staff = (Staff)session.get(Staff.class,1);
		session.delete(staff);

		session.getTransaction().commit();
		session.close();
	}

	@Test
	public void testMappedBy(){


	}
}
